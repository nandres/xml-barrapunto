#!/usr/bin/python2

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string






class myCounterHandler(ContentHandler):

    def __init__ (self):
        self.inItem = 0
        self.inContent = 0
        self.theContent = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inItem = 1
        elif self.inItem:
            if name == 'title':
                self.inContent = 1
            elif name == 'link':
                self.inContent = 1
    def endElement (self, name):

        if name == 'item':
            self.inItem = 0
        elif self.inItem:
            if name == 'tittle':
                print "  Tittle: " + self.theContent + "."
                print line.encode('utf-8')
                self.inContent = 0
                self.theContent = ""

            elif name == 'link':
                print "  link: " + self.theContent + "."
                self.inContent = 0
                self.theContent = ""




    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog

if len(sys.argv)<2:
    print "Usage: python xml-parser-jokes.py <document>"
    print
    print " <document>: file name of the document to parse"
    sys.exit(1)

# Load parser and driver

JokeParser = make_parser()
JokeHandler = myCounterHandler()
JokeParser.setContentHandler(JokeHandler)

# Ready, set, go!

xmlFile = open(sys.argv[1],"r")
JokeParser.parse(xmlFile)

print "Parse complete"
